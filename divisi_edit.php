<?php 
  include("component/header-config.php");
  include("component/header.php");
  include("component/sidebar.php"); 
?>
  <div class="content-wrapper">
    <section class="content container-fluid">
      <div class="row">
        <div class="col-md-6">
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Data Karyawan</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="divisi_edit.php" method="POST" role="form">
              <?php
                $id='';
                $nama='';
                $getfile_divisi = file_get_contents('json-data/divisi.json');
                $jsonfile_divisi = json_decode($getfile_divisi,true);

              if(isset($_POST["cari"]) && isset($_POST["divisi_id"])){
                  $id=$_POST["divisi_id"];
                  for($i=0;$i<count($jsonfile_divisi);$i++){
                      if($jsonfile_divisi[$i]['divisi_id']==$id){
                          $nama=$jsonfile_divisi[$i]['divisi_name'];
                          $i=count($jsonfile_divisi);
                      }
                  }
              }else if(isset($_POST["ubah"]) && isset($_POST["divisi_id"])){
                  $id=$_POST["divisi_id"];
                  for($i=0;$i<count($jsonfile_divisi);$i++){
                      if($jsonfile_divisi[$i]['divisi_id']==$id){
                          $jsonfile_divisi[$i]['divisi_name'] = $_POST["divisi_name"];
                          $i=count($jsonfile_divisi);
                      }
                  }
                  file_put_contents("json-data/divisi.json", json_encode($jsonfile_divisi));
                  header("Location: divisi_edit.php");
              }else if(isset($_POST["hapus"]) && isset($_POST["divisi_id"])){
                  $id=$_POST["divisi_id"];
                  $index_baru=0;
                  $data_baru = array();
                  for($i=0;$i<count($jsonfile_divisi);$i++){
                      if($jsonfile_divisi[$i]['divisi_id']==$id){
                          $jsonfile_divisi[$i]['divisi_name'] = $_POST["divisi_name"];
                      }else{
                          $array_temp = array("divisi_id"=>$jsonfile_divisi[$i]["divisi_id"],"divisi_name"=>$jsonfile_divisi[$i]["divisi_name"]);
                          $data_baru[$index_baru]=$array_temp;
                          $index_baru++;
                      }
                  }
                  file_put_contents("json-data/divisi.json", json_encode($data_baru));
                  header("Location: divisi_edit.php");
              }

              ?>
              <div class="box-body">
               
                <div class="form-group">
                  <label for="exampleInputEmail1">Masukkan ID Divisi</label>
                  <input value="<?php echo $id ?>" name="divisi_id" type="text" class="form-control" id="exampleInputEmail1" placeholder="Masukkan ID..." >
                </div>
                
                <button type="submit" name="cari" value="Cari" class="btn btn-primary">Cari Divisi</button>

                <div class="form-group">
                  <label for="exampleInputPassword1">Nama Divisi</label>
                  <input value="<?php echo $nama ?>" name="divisi_name" type="text" class="form-control" id="exampleInputPassword1" placeholder="Nama Lengkap...">
                </div>

              </div>
              <div class="box-footer">
                <input type="submit" name="ubah" value="Ubah" class="btn btn-primary" />
                <input type="submit" name="hapus" value="Hapus" class="btn btn-danger"/>
               
              </div>
            </form>
           
          </div>
        </div>

        <?php include("divisi_json.php"); ?>
        
      </div>

      
    </section>
  </div>
<?php 
  include("component/footer.php");
?>
