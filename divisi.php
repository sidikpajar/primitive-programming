<?php 
  include("component/header-config.php");
  include("component/header.php");
  include("component/sidebar.php"); 
?>
  <div class="content-wrapper">
    <section class="content container-fluid">
      <div class="row">
        <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Data Divisi</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="divisi.php" method="POST" role="form">
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Generate Divisi ID</label>
                  <input name="divisi_id" type="text" class="form-control" id="exampleInputEmail1" placeholder="Masukkan ID..." >
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Nama Divisi</label>
                  <input name="divisi_name" type="text" class="form-control" id="exampleInputPassword1" placeholder="Nama depan...">
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" name="submit" value="submit" class="btn btn-primary">Tambah</button>
                <a href="divisi_edit.php" class="btn btn-warning">Cari</a>
              </div>
            </form>
            <?php
              if  (isset($_POST["submit"])) {
                    $file = file_get_contents('json-data/divisi.json');
                    $data = json_decode($file, true);
                    unset($_POST["submit"]);
                    $array_temp = array("divisi_id"=>$_POST["divisi_id"],
                                        "divisi_name"=>$_POST["divisi_name"]);
                    $data[count($data)]=$array_temp;
                    file_put_contents("json-data/divisi.json", json_encode($data));
                  
                    // error_reporting(E_ALL);
                    // ini_set("display_errors", 1);
                  }
            ?>
          </div>
        </div>

        <?php include("divisi_json.php"); ?>

      </div>

      
    </section>
  </div>
<?php 
  include("component/footer.php");
?>
