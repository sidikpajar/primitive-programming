<header class="main-header">
    <a href="#" class="logo">
      <span class="logo-mini"><b>M</b>Kom</span>
      <span class="logo-lg"><b>Struktur</b> Data</span>
    </a>
    <nav class="navbar navbar-static-top" role="navigation">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
    </nav>
  </header>