<?php 
  include("component/header-config.php");
  include("component/header.php");
  include("component/sidebar.php"); 

  $getfile = file_get_contents('json-data/karyawan.json');
  $jsonfile = json_decode($getfile,true);

  $getfile_divisi = file_get_contents('json-data/divisi.json');
  $jsonfile_divisi = json_decode($getfile_divisi,true);
  
  $URUTAN='1';
  if(isset($_POST["urutan"])){
      $URUTAN=$_POST["urutan"];
  }
  if($URUTAN=='1' || $URUTAN=='2' || $URUTAN=='4'){
      $KUNCI_INDEX = 'employee_id';
      if($URUTAN=='2'){
          $KUNCI_INDEX = 'employee_name';
      }else if($URUTAN=='4'){
          $KUNCI_INDEX = 'employee_ip';
      }
      for($i=0;$i<count($jsonfile);$i++){
          for($j=($i+1);$j<count($jsonfile);$j++){
              if($jsonfile[$j][$KUNCI_INDEX]<$jsonfile[$i][$KUNCI_INDEX]){
                  $array_temp = array("employee_id"=>$jsonfile[$i]["employee_id"],"employee_name"=>$jsonfile[$i]["employee_name"],"divisi_id"=>$jsonfile[$i]["divisi_id"],"employee_ip"=>$jsonfile[$i]["employee_ip"]);
                  $jsonfile[$i] = array("employee_id"=>$jsonfile[$j]["employee_id"],"employee_name"=>$jsonfile[$j]["employee_name"],"divisi_id"=>$jsonfile[$j]["divisi_id"],"employee_ip"=>$jsonfile[$j]["employee_ip"]);
                  $jsonfile[$j] = $array_temp;

              }
          }
      }
  }else{
      for($i=0;$i<count($jsonfile);$i++){
          for($j=($i+1);$j<count($jsonfile);$j++){
              $id_divisi1 = $jsonfile[$i]["divisi_id"];
              $id_divisi2 = $jsonfile[$j]["divisi_id"];

            $nama_divisi1 = '';
            $nama_divisi2 = '';
            $flag1=false;
            $flag2=false;
            for($k=0;$k<count($jsonfile_divisi);$k++){
                if($id_divisi1==$jsonfile_divisi[$k]['divisi_id']){
                    $nama_penerbit1 = $jsonfile_divisi[$k]['divisi_name'];
                    $flag1=true;
                }
                if($id_divisi2==$jsonfile_divisi[$k]['divisi_id']){
                    $nama_divisi2 = $jsonfile_divisi[$k]['divisi_name'];
                    $flag2=true;
                }
                if($flag1 && $flag2){
                    $k=count($jsonfile_divisi);
                }
            }

            if($nama_divisi2<$nama_divisi1){
                $array_temp = array("employee_id"=>$jsonfile[$i]["employee_id"],"employee_name"=>$jsonfile[$i]["employee_name"],"divisi_id"=>$jsonfile[$i]["divisi_id"],"employee_ip"=>$jsonfile[$i]["employee_ip"]);
                $jsonfile[$i] = array("employee_id"=>$jsonfile[$j]["employee_id"],"employee_name"=>$jsonfile[$j]["employee_name"],"divisi_id"=>$jsonfile[$j]["divisi_id"],"employee_ip"=>$jsonfile[$j]["employee_ip"]);
                $jsonfile[$j] = $array_temp;

            }
        }
    }
}

?>


  <div class="content-wrapper">
    <section class="content container-fluid">
      <div class="row">

        <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Data Karyawan</h3>
            </div>
            <div class="box-body">
              <form action="index.php" method="POST">
                <table cellpadding="10">
                  <tr>
                    <td>Urut berdasarkan</td>
                    <td>
                      <select name="urutan" onchange="this.form.submit()">
                        <?php 
                          if($URUTAN=='1'){
                        ?>
                          <option value="1" SELECTED>ID Karyawan</option>
                          <option value="2">Nama Karyawan</option>
                          <option value="3">Divisi</option>
                          <option value="4">Nama Akhir</option>
                        <?php
                          }else if($URUTAN=='2'){
                        ?>
                          <option value="1" >ID Karyawan</option>
                          <option value="2" SELECTED>Nama Karyawan</option>
                          <option value="3">Divisi</option>
                          <option value="4">Nama Akhir</option>
                        <?php
                          }else if($URUTAN=='3'){
                        ?>
                          <option value="1" >ID Karyawan</option>
                          <option value="2" >Nama Karyawan</option>
                          <option value="3" SELECTED>Divisi</option>
                          <option value="4">Nama Akhir</option>
                        <?php
                          }else{
                        ?>
                          <option value="1" >ID Karyawan</option>
                          <option value="2" >Nama Karyawan</option>
                          <option value="3" >Divisi</option>
                          <option value="4" SELECTED>Nama Akhir</option>
                        <?php
                          }
                        ?>
                      </select> 
                    </td>
                  </tr>
                </table>
              </form>
            
            
            <!-- /.box-header -->
          
              <table class="table table-bordered">
                <tbody>
                <tr>
                  <th>ID Karyawan</th>
                  <th>Nama Karyawan</th>
                  <th>Divisi</th>
                  <th>Indeks Prestasi</th>
                </tr>
                <?php for($i=0;$i<count($jsonfile);$i++){ ?>
                <tr>
                  <td><?php echo $jsonfile[$i]['employee_id'] ?></td>
                  <td><?php echo $jsonfile[$i]['employee_name'] ?></td>
                      <?php
                          $id_divisi = $jsonfile[$i]['divisi_id'];
                          $nama_divisi = '';
                          for($j=0;$j<count($jsonfile_divisi);$j++){
                              if($id_divisi==$jsonfile_divisi[$j]['divisi_id']){
                                  $nama_divisi = $jsonfile_divisi[$j]['divisi_name'];
                                  $j=count($jsonfile_divisi);
                              }
                          } 
                      ?>
                  <td><?php echo $nama_divisi ?></td>
                  <td>
                    <?php $dia=$jsonfile[$i]['employee_ip']; ?>
                    <div class="progress progress-xs">
                      <?php echo "<div class='progress-bar progress-bar-danger' style='width:".$dia."%' " ?>
                    </div>
                    
                  </td>
                </tr>
                <?php } ?>
              </tbody>
              </table>



           
            </div>
          </div>




          
        <!-- End Col-md-6 -->
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
            
              <div class="box-header with-border">
                <h3 class="box-title">Tugas Kelompok</h3>
              </div>
              <div class="box-body">
            
                <table class="table table-bordered">
                  <tbody>
                  <tr>
                    <th>Nomor Mahasiswa</th>
                    <th>Nama Mahasiswa</th>
                  </tr>
                  <tr>
                    <td>1811601051</td>
                    <td>Irwan susanto</td>
                  </tr>
                  <tr>
                    <td>1811601119</td>
                    <td>M. Fajar Sidik</td>
                  </tr>
                </tbody>
                </table>
              </div>

            </div>
          </div>
        
      
        <!-- End Row -->

      
      
      

      
    </section>
  </div>
<?php 
  include("component/footer.php");
?>
