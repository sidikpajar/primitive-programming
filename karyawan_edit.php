<?php 
  include("component/header-config.php");
  include("component/header.php");
  include("component/sidebar.php"); 
?>
  <div class="content-wrapper">
    <section class="content container-fluid">
      <div class="row">
        <div class="col-md-6">
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Data Karyawan</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="karyawan_edit.php" method="POST" role="form">
              <?php
                $id='';
                $judul='';
                $divisi='';
                $keterangan='';
                $getfile_divisi = file_get_contents('json-data/divisi.json');
                $jsonfile_divisi = json_decode($getfile_divisi,true);

               

                if(isset($_POST["cari"]) && isset($_POST["employee_id"])){
                  $id=$_POST["employee_id"];
                  $getfile = file_get_contents('json-data/karyawan.json');
                  $jsonfile = json_decode($getfile,true);
                  for($i=0;$i<count($jsonfile);$i++){
                      if($jsonfile[$i]['employee_id']==$id){
                          $judul=$jsonfile[$i]['employee_name'];
                          $divisi=$jsonfile[$i]['divisi_id'];
                          $keterangan=$jsonfile[$i]['employee_ip'];
                          $i=count($jsonfile);
                      }
                  }
              }else if(isset($_POST["ubah"]) && isset($_POST["employee_id"])){
                  $id=$_POST["employee_id"];
                  $getfile = file_get_contents('json-data/karyawan.json');
                  $jsonfile = json_decode($getfile,true);
                  for($i=0;$i<count($jsonfile);$i++){
                      if($jsonfile[$i]['employee_id']==$id){
                          $jsonfile[$i]['employee_name'] = $_POST["employee_name"];
                          $jsonfile[$i]['divisi_id'] = $_POST["divisi_id"];
                          $jsonfile[$i]['employee_ip'] = $_POST["employee_ip"];
                          $i=count($jsonfile);
                      }
                  }
                  file_put_contents("json-data/karyawan.json", json_encode($jsonfile));
                  header("Location: karyawan_edit.php");
              }else if(isset($_POST["hapus"]) && isset($_POST["employee_id"])){
                  $id=$_POST["employee_id"];
                  $getfile = file_get_contents('json-data/karyawan.json');
                  $jsonfile = json_decode($getfile,true);
                  $index_baru=0;
                  $data_baru = array();
                  for($i=0;$i<count($jsonfile);$i++){
                      if($jsonfile[$i]['employee_id']==$id){
                          $jsonfile[$i]['employee_name'] = $_POST["employee_name"];
                          $jsonfile[$i]['divisi_id'] = $_POST["divisi_id"];
                          $jsonfile[$i]['employee_ip'] = $_POST["employee_ip"];
                      }else{
                          $array_temp = array("employee_id"=>$jsonfile[$i]["employee_id"],"employee_name"=>$jsonfile[$i]["employee_name"],"divisi_id"=>$jsonfile[$i]["divisi_id"],"employee_ip"=>$jsonfile[$i]["employee_ip"]);
                          $data_baru[$index_baru]=$array_temp;
                          $index_baru++;
                      }
                  }
                  file_put_contents("json-data/karyawan.json", json_encode($data_baru));
                  header("Location: karyawan_edit.php");
              }

              ?>
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Generate ID Karyawan</label>
                  <input value="<?php echo $id ?>" name="employee_id" type="text" class="form-control" id="exampleInputEmail1" placeholder="Masukkan ID..." >
                </div>
                
                <button type="submit" name="cari" value="Cari" class="btn btn-primary">Cari</button>

                <div class="form-group">
                  <label for="exampleInputPassword1">Nama Lengkap</label>
                  <input value="<?php echo $judul ?>" name="employee_name" type="text" class="form-control" id="exampleInputPassword1" placeholder="Nama Lengkap...">
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Divisi</label>
                  <select name="divisi_id" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true">
 

                    <?php  
                      for($i=0;$i<count($jsonfile_divisi);$i++){
                        if(isset($_POST["cari"]) && $divisi==$jsonfile_divisi[$i]['divisi_id']){
                            ?>
                            <option value="<?php echo $jsonfile_divisi[$i]['divisi_id'] ?>" SELECTED><?php echo $jsonfile_divisi[$i]['divisi_name'] ?></option>
                            <?php
                        }else{
                            ?>
                            <option value="<?php echo $jsonfile_divisi[$i]['divisi_id'] ?>"><?php echo $jsonfile_divisi[$i]['divisi_name'] ?></option>
                            <?php
                        }
                      }
                    ?>
                  
                  </select>
                </div>
                
                <div class="form-group">
                  <label for="exampleInputPassword1">Indeks Prestasi</label>
                  <input value="<?php echo $keterangan ?>" name="employee_ip" type="number" class="form-control" id="exampleInputPassword1" placeholder="0-100">
                </div>

              </div>
              <div class="box-footer">
                <input type="submit" name="ubah" value="Ubah" class="btn btn-primary" />
                <input type="submit" name="hapus" value="Hapus" class="btn btn-danger"/>
                <a href="karyawan_edit.php">Back</a>
              </div>
            </form>
            <?php if (isset($_GET["id"])): ?>
            <?php endif; ?>
          </div>
        </div>

        <?php include("karyawan_json.php"); ?>


      
    </section>
  </div>
<?php 
  include("component/footer.php");
?>
