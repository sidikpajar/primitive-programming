<?php 
  include("component/header-config.php");
  include("component/header.php");
  include("component/sidebar.php"); 
?>
  <div class="content-wrapper">
    <section class="content container-fluid">
      <div class="row">
        <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Data Karyawan</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="karyawan.php" method="POST" role="form">
              <?php
                $data_divisi = file_get_contents('json-data/divisi.json');
                $json_divisi = json_decode($data_divisi,true);
              ?>
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Generate ID Karyawan</label>
                  <input name="employee_id" type="text" class="form-control" id="exampleInputEmail1" placeholder="Masukkan ID..." >
                </div>
                <div class="form-group">
                  <label for="exampleInputEmail1">Divisi</label>
                  <select name="divisi_id" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true">
                    <?php  
                      for($i=0;$i<count($json_divisi);$i++){
                      ?>
                      <option value="<?php echo $json_divisi[$i]['divisi_id'] ?>">
                      <?php echo $json_divisi[$i]['divisi_name'] ?> </option>
                      <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Nama Lengkap</label>
                  <input name="employee_name" type="text" class="form-control" id="exampleInputPassword1" placeholder="Nama Lengkap...">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Indeks Prestasi</label>
                  <input name="employee_ip" type="number" class="form-control" id="exampleInputPassword1" placeholder="0-100">
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" name="submit" value="submit" class="btn btn-primary">Tambah</button>
                <a href="karyawan_edit.php" class="btn btn-warning">Cari</a>
              </div>
            </form>
            <?php
              if  (isset($_POST["submit"])) {
                    $file = file_get_contents('json-data/karyawan.json');
                    $data = json_decode($file, true);
                    unset($_POST["submit"]);
                    $array_temp = array("employee_id"=>$_POST["employee_id"],
                                        "divisi_id"=>$_POST["divisi_id"],
                                        "employee_name"=>$_POST["employee_name"],
                                        "employee_ip"=>$_POST["employee_ip"]);
                    $data[count($data)]=$array_temp;
                    file_put_contents("json-data/karyawan.json", json_encode($data));
                  
                    // error_reporting(E_ALL);
                    // ini_set("display_errors", 1);
                  }
            ?>
          </div>
        </div>

        <?php include("karyawan_json.php"); ?>



      </div>

      
    </section>
  </div>
<?php 
  include("component/footer.php");
?>
