<div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Data JSON Divisi</h3>
            </div>
            <div class="box-body with-border" style="font-size:8px">
              <?php
                $str_divisi = file_get_contents('json-data/divisi.json');
                $json_divisi = json_decode($str_divisi, true); // decode the JSON into an associative array
                echo '<pre style="font-size:10px">' . print_r($json_divisi, true) . '</pre>';
              ?>
              <a class="btn btn-success" href="json-data/divisi.json" download> Download File Json Divisi </a>
            </div>
          </div>
        </div>